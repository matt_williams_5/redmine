variable "key_name" {
  default = "docker-test"
}

variable "public_key" {
  default = "/Users/mattw/.ssh/docker-test"
}

variable "RDS_PASSWORD" {}
