resource "aws_launch_configuration" "lc" {
  name_prefix     = "lc-"
  image_id        = "ami-15e9c770"
  instance_type   = "t2.micro"
  security_groups = ["${aws_security_group.allow-access.id}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  name                      = "remine_asg"
  max_size                  = 4
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 2
  force_delete              = true
  vpc_zone_identifier       = ["${aws_subnet.redmine-public-1.id}", "${aws_subnet.redmine-private-1.id}"]
  launch_configuration      = "${aws_launch_configuration.lc.id}"

  tag {
    key                 = "Name"
    value               = "asg-instance"
    propagate_at_launch = true
    }

  lifecycle {
    create_before_destroy = true
  }
}
