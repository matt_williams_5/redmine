# Redmine

Redmine is a flexible project management web application. Written using the Ruby on Rails framework, it is cross-platform and cross-database.

## Getting Started

This is a simple deployment for the Redmine application

### Prerequisites

What things you need to install the software and how to install them

```
terraform must be installed(https://www.terraform.io/intro/getting-started/install.html) and you'll need to update your .aws creds. (https://docs.aws.amazon.com/cli/latest/userguide/cli-config-files.html)
```

## Deployment

- terraform init
- terraform plan -var RDS_PASSWORD=password
- terraform apply -var RDS_PASSWORD=password

## Author

* **Matt Williams** - http://www.github.com/mattwilliams5
