resource "aws_security_group" "allow-access" {
  vpc_id      = "${aws_vpc.redmine-default.id}"
  name        = "allow-connection"
  description = "security group that allows ssh, tcp port 3000 and all egress traffic"
  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow-ssh"
  }

  ingress {
      from_port   = 3000
      to_port     = 3000
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow-connections"
  }
}

resource "aws_security_group" "allow-mysql" {
  vpc_id              = "${aws_vpc.redmine-default.id}"
  name                = "allow-mysql"
  description         = "allow-mysql"
  ingress {
      from_port       = 3306
      to_port         = 3306
      protocol        = "tcp"
      security_groups = ["${aws_security_group.allow-access.id}"]
  }
  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      self        = true
  }
  tags {
    Name = "allow-mysql"
  }
}
