# Internet VPC
resource "aws_vpc" "redmine-default" {
    cidr_block           = "10.0.0.0/16"
    instance_tenancy     = "default"
    enable_dns_support   = "true"
    enable_dns_hostnames = "true"
    enable_classiclink   = "false"
    tags {
        Name = "redmine-default"
    }
}

# Subnets
resource "aws_subnet" "redmine-public-1" {
    vpc_id                  = "${aws_vpc.redmine-default.id}"
    cidr_block              = "10.0.1.0/24"
    map_public_ip_on_launch = "true"
    availability_zone       = "us-east-2b"

    tags {
        Name = "redmine-public-1"
    }
}

resource "aws_subnet" "redmine-public-2" {
    vpc_id                  = "${aws_vpc.redmine-default.id}"
    cidr_block              = "10.0.2.0/24"
    map_public_ip_on_launch = "true"
    availability_zone       = "us-east-2c"

    tags {
        Name = "redmine-public-2"
    }
}

resource "aws_subnet" "redmine-private-1" {
    vpc_id                  = "${aws_vpc.redmine-default.id}"
    cidr_block              = "10.0.4.0/24"
    map_public_ip_on_launch = "false"
    availability_zone       = "us-east-2b"

    tags {
        Name = "redmine-private-1"
    }
}

resource "aws_subnet" "redmine-private-2" {
    vpc_id                  = "${aws_vpc.redmine-default.id}"
    cidr_block              = "10.0.5.0/24"
    map_public_ip_on_launch = "false"
    availability_zone       = "us-east-2c"

    tags {
        Name = "redmine-private-2"
    }
}


# Internet GW
resource "aws_internet_gateway" "redmine-gw" {
    vpc_id   = "${aws_vpc.redmine-default.id}"

    tags {
        Name = "redmine"
    }
}

# route tables
resource "aws_route_table" "redmine-public" {
    vpc_id         = "${aws_vpc.redmine-default.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.redmine-gw.id}"
    }

    tags {
        Name = "redmine-public-1"
    }
}

# route associations public
resource "aws_route_table_association" "redmine-public-1-a" {
    subnet_id      = "${aws_subnet.redmine-public-1.id}"
    route_table_id = "${aws_route_table.redmine-public.id}"
    }
    
resource "aws_route_table_association" "redmine-public-2-a" {
    subnet_id      = "${aws_subnet.redmine-public-2.id}"
    route_table_id = "${aws_route_table.redmine-public.id}"
}
