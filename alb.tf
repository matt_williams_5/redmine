resource "aws_lb" "redmine_lb" {
  name               = "redmine-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.allow-access.id}"]
  subnets            = ["${aws_subnet.redmine-public-1.id}",  "${aws_subnet.redmine-public-2.id}"]

  enable_deletion_protection = false

  tags {
    Environment = "production"
  }
}
