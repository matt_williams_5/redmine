resource "aws_db_subnet_group" "mysql-subnet" {
    name        = "mysql-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${aws_subnet.redmine-private-1.id}","${aws_subnet.redmine-private-2.id}"]
}

resource "aws_db_parameter_group" "mysql-parameters" {
    name = "mysql-parameters"
    family = "mysql5.7"
    description = "mysql parameter group"

    parameter {
      name = "max_allowed_packet"
      value = "16777216"
   }

}
# Create a database server
resource "aws_db_instance" "redmine" {
  identifier               = "redmine"
  allocated_storage        = 20
  storage_type             = "gp2"
  engine                   = "mysql"
  engine_version           = "5.7.16"
  instance_class           = "db.t2.micro"
  name                     = "redmine"
  username                 = "root"
  password                 = "${var.RDS_PASSWORD}"
  publicly_accessible      = true
  #backup_retention_period = 30
  vpc_security_group_ids   = ["${aws_security_group.allow-mysql.id}"]
  db_subnet_group_name     = "${aws_db_subnet_group.mysql-subnet.name}"
  parameter_group_name     = "${aws_db_parameter_group.mysql-parameters.name}"
  availability_zone        = "${aws_subnet.redmine-private-1.availability_zone}"
  skip_final_snapshot      = true
  multi_az                 = "false"
}
