output "instance" {
  value = "${aws_instance.redmine.public_ip}"
}

output "rds" {
  value = "${aws_db_instance.redmine.endpoint}"
}
