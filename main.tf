provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "redmine" {
  ami                    = "ami-15e9c770"
  subnet_id              = "${aws_subnet.redmine-public-1.id}"
  instance_type          = "t2.micro"
  key_name               = "${var.key_name}"
  vpc_security_group_ids = [ "${aws_security_group.allow-access.id}" ]
  depends_on             = ["aws_db_instance.redmine"]

lifecycle {
    create_before_destroy = true
  }

connection {
   user        = "ec2-user"
   type        = "ssh"
   private_key = "${file(var.public_key)}"
 }

provisioner "file" {
  source      = "conf/docker-compose.yml"
  destination = "/home/ec2-user/docker-compose.yml"
}

 provisioner "remote-exec" {
               inline = [ "sudo yum install -y docker mysql57",
                 "sudo service docker start",
                 "sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose",
                 "sudo chmod +x /usr/local/bin/docker-compose",
                 "sudo /usr/local/bin/docker-compose up -d" ]
  }
}
